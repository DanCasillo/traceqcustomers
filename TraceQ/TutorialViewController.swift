//
//  TutorialViewController.swift
//  TraceQ
//
//  Created by Francesco Aroldo on 16/06/2020.
//  Copyright © 2020 Danilo Casillo. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController, UIScrollViewDelegate {
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    var slides:[tutorialView] = [];
    
    var ud = UserDefaults.standard
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        scrollView.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(red: 255/255, green: 205/255, blue: 145/255, alpha: 1)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        slides = createSlides()
        setupSlideScrollView(slides: slides)
        
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        view.bringSubviewToFront(pageControl)
    }
    
    func setupSlideScrollView(slides : [tutorialView]) {
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.height)
        scrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
            scrollView.addSubview(slides[i])
        }
    }
    
    func createSlides() -> [tutorialView] {
        
        let slide1:tutorialView = Bundle.main.loadNibNamed("tutorial", owner: self, options: nil)?.first as! tutorialView
        slide1.image.image = UIImage(named: "tutorial-1")
        slide1.title.text = "Book your spot"
        slide1.label.text = "You can select the restaurant you want to go to from the list and tap on the Book button to join the queue."
        slide1.button.alpha = 0
        
        let slide2:tutorialView = Bundle.main.loadNibNamed("tutorial", owner: self, options: nil)?.first as! tutorialView
        slide2.image.image = UIImage(named: "tutorial-2")
        slide2.title.text = "Scan your spot"
        slide2.label.text = "If you already are in front of the restaurant you like, scan the QR code posted outside in order to join the queue."
        slide2.button.alpha = 0
        
        
        let slide3:tutorialView = Bundle.main.loadNibNamed("tutorial", owner: self, options: nil)?.first as! tutorialView
        slide3.image.image = UIImage(named: "tutorial-3")
        slide3.title.text = "Check your booking"
        slide3.label.text = "Tap on the Queue button in order to have a look on the status of your queue."
        slide3.button.alpha = 0
        
        let slide4:tutorialView = Bundle.main.loadNibNamed("tutorial", owner: self, options: nil)?.first as! tutorialView
        slide4.image.image = UIImage(named: "tutorial4")
        slide4.title.text = "Notification System"
        slide4.label.text = "You will be notified by TraceQ ten minutes before your turn so you don’t miss your booking."
        slide4.button.alpha = 1
        slide4.button.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
        
        return [slide1, slide2, slide3, slide4]
    }
    
    @objc func buttonAction(sender: UIButton){
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "Loading") as? LoadingViewController
        self.view.window?.rootViewController = vc
        UserDefaults.standard.set(false, forKey: "firstLaunch")
        
    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        
    }
}
