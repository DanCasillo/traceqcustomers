//
//  QRViewController.swift
//  TraceQ
//
//  Created by Michele Garofalo on 14/05/2020.
//  Copyright © 2020 Danilo Casillo. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase

class QRViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    @IBOutlet weak var scan: UIImageView!
    @IBOutlet weak var front: UIImageView!
    @IBOutlet weak var dismissButton: UIButton!
    
    var codeActivity : String!
    var nameActivity : String!
    var check : Bool = false
    var captureSession = AVCaptureSession()
    
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    
    var qrCodeFrameView: UIView?
    
    var attivita = ["Ristoranti", "Pizzerie", "Pub"]
    
    private let supportedCodeTypes = [AVMetadataObject.ObjectType.qr]
    
    override func viewDidAppear(_ animated: Bool) {
        reset()
         NotificationCenter.default.addObserver(self, selector: #selector(showAlert), name: Notification.Name("showAlert"), object: nil)
        dismissButton.layer.cornerRadius = dismissButton.frame.height/6
    }
    
    @objc func showAlert(){
           let alert = AlertServices()
           self.present(alert.alert("ConnectionAlert"), animated: true, completion: nil)
       }
       
  override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("showAlert"), object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        guard let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) else {
            return
        }
        
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            captureSession.addInput(input)
            
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
            
        } catch {
            return
        }
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        
        view.bringSubviewToFront(front)
        view.bringSubviewToFront(scan)
        view.addSubview(dismissButton)
        captureSession.startRunning()
        qrCodeFrameView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
   
    @IBAction func dismissAction(_ sender: Any) {
    
        self.dismiss(animated: true, completion: nil)
    }
    
    func launchApp() {
        
        scan.isHidden = true
        
        if presentedViewController != nil {
            return
        }
        
        
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "booking"), object: nil, userInfo: ["Code" : self.codeActivity])})
        /*checkCode(completion: {isFinished in
            if (isFinished){
                print("finito")
                if self.check {
                    
                }
                else {
                    let alert = UIAlertController(title: "ERROR!", message: "QR code not recognized", preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {(action) in
                        self.reset()
                    } )
                    
                    let homeAction = UIAlertAction(title: "Home", style: UIAlertAction.Style.default, handler: { (action) in
                        //                CODICE PER RITORNARE ALLA HOME
                        self.navigationController?.popToRootViewController(animated: true)
                    })
                    
                    alert.addAction(homeAction)
                    alert.addAction(cancelAction)
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }
        })
        */
    }
    
    func reset(){
        self.captureSession.startRunning()
        self.scan.isHidden = false
        self.check = false
    }
    
    func checkCode(completion : @escaping (Bool) -> Void) {
        
        let reference = Database.database().reference().child("Attività")
        
//        var i: Int = 0
        
        reference.observe(.value, with: {snapshot in
            for child in snapshot.children {
                let snap = child as! DataSnapshot
                
                for child in snap.children {
                    let snap = child as! DataSnapshot
                    if self.codeActivity == snap.key {
                        print("check change")
                        self.check = true
                        self.nameActivity = ((snap.value as! NSDictionary)["Nome"] as! String)
                    }
                }
            }
            print("fuori")
            completion(true)
        })
        
        
        print("outside")
        
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    /*    let button = sender as! UIButton
        let segue = (segue as! OHCircleSegue)
        segue.destination.modalPresentationStyle = .fullScreen
        segue.circleOrigin = button.center*/
    }
    
    
   
    
}

extension QRViewController {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            return
        }
        
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                captureSession.stopRunning()
                codeActivity = metadataObj.stringValue!
                launchApp()
            }
        }
    }
    
}





