//
//  LeaveQueueViewController.swift
//  TraceQ
//
//  Created by Danilo Casillo on 02/06/2020.
//  Copyright © 2020 Danilo Casillo. All rights reserved.
//

import UIKit
import Firebase
class LeaveQueueViewController: UIViewController {
    
    @IBOutlet weak var TFName: UITextField!
    @IBOutlet weak var numPeople: UILabel!
    @IBOutlet weak var yourTurn: UILabel!
    
    var booking : Booking!
    var ref : DatabaseReference!
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(showAlert), name: Notification.Name("showAlert"), object: nil)
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("showAlert"), object: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference().child("Prenotazione").child(booking.codeActivity!)
        
        TFName.text = booking.nameBooker!
        numPeople.text = String(booking.numPeople)
        
        loadBooking(completion: {(isFinished, count) in
            self.yourTurn.text = String(count)
            
        })
        
        // Do any additional setup after loading the view.
    }
    
    @objc func showAlert(){
        let alert = AlertServices()
        self.present(alert.alert("ConnectionAlert"), animated: true, completion: nil)
    }
    
    func loadBooking(completion: @escaping (Bool,Int) -> Void) {
        
        
        ref.observe(.value, with: {snapshot in
            
            var trovato : Bool = false
            var count : Int = 0
            
            let childs = snapshot.children.allObjects as! [DataSnapshot]
            
            guard let book = self.booking else {
                return
            }
            
            while trovato == false && count < childs.count {
                print(childs[count].key)
                
                if childs[count].key == book.codeBooking! {
                    trovato = true
                }
                count += 1
            }
            completion(true,count)
            
        })
    }
    
    @IBAction func leaveQ(_ sender: Any) {
        NotificationCenter.default.addObserver(self, selector: #selector(leaveConfirmed), name: Notification.Name(rawValue: "leavingConfirmed"), object: nil)
        let alert = AlertServices()
        self.present(alert.alert("leaveQAlert"),animated: true, completion: nil)
    }
    
    @objc func leaveConfirmed(){
        ref.removeAllObservers()
        ref.child(booking.codeBooking).removeValue(completionBlock: {(error,ref) in
            if let error = error {
                print(error)
            } else {
                self.booking = nil
            }
        })
        
        UserDefaults.standard.removeObject(forKey: "isBooked")
        self.navigationController?.popToRootViewController(animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
