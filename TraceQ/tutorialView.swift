//
//  tutorialView.swift
//  TraceQ
//
//  Created by Francesco Aroldo on 16/06/2020.
//  Copyright © 2020 Danilo Casillo. All rights reserved.
//

import UIKit

class tutorialView: UIView {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var button: UIButton!
    
}
