//
//  ViewController.swift
//  TraceQ
//
// It contains all activities (restaurants, pizzerie e pub) 


import UIKit
import Firebase

class ViewController: UIViewController {
    
    
    //UI elements
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var QRButton: UIButton!
    var tapped : Bool = false
    var cellTapped : Int!
    //Data elements
    var ref: DatabaseReference!
    var storage : StorageReference!
    var filterApplied : Bool = false
    var activities = [Activity]()
    var filteredActivity = [Activity]()
    var searchedActivity = [Activity]()
    var codeActivity : String!
    var nameActivity : String!
    var onlineActivity : Bool!
    //Filter view
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var check: UIImageView!
    @IBOutlet weak var buttonFilter: UIButton!
    
    var searchBar : UISearchBar!
    
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.joinQ), name: Notification.Name("booking"), object: nil)
        
        
        QRButton.CreateFloatingButton()
        
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        ref = Database.database().reference()
        storage = Storage.storage().reference()
        
        //initFilterView
        
        filterView.layer.cornerRadius = 10
        filterView.layer.borderColor = UIColor.orange.cgColor
        filterView.layer.borderWidth = 1.0
        filterView.clipsToBounds = true
        
        searchedActivity = activities
        
        /*Init navbar*/
        searchBar = UISearchBar()
        searchBar.sizeToFit()
        searchBar.placeholder = "Search"
        searchBar.searchTextField.backgroundColor = .white
        searchBar.delegate = self
        searchBar.searchTextField.delegate = self
        searchBar.returnKeyType = .done
        searchBar.enablesReturnKeyAutomatically = false
        tableview.keyboardDismissMode = .onDrag
        navigationItem.titleView = searchBar
        
        
        
        tableview.delegate = self
        tableview.dataSource = self
        tableview.tableFooterView = UIView()
        tableview.alwaysBounceVertical = false
        
        tableview.keyboardDismissMode = .interactive
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(showAlert), name: Notification.Name("showAlert"), object: nil)
        tapGesture()
    }
    private func tapGesture(){
        let tapGesturez = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tapGesturez.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesturez)
    }
    
    @objc func handleTap(){
        view.endEditing(true)
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("showAlert"), object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        button.setImage(UIImage(named: "queueButton"), for: .normal)
        button.addTarget(self, action: #selector(showBooks), for: .touchUpInside)
        
        let barButton = UIBarButtonItem(customView: button)
        
        barButton.image?.withRenderingMode(.alwaysOriginal)
        if (UserDefaults.standard.data(forKey: "Booking") == nil ) {
            barButton.customView?.isUserInteractionEnabled = false
            barButton.customView?.alpha = 0.5
        } else {
            let booking = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.data(forKey: "Booking")!) as? Booking
            print(booking?.codeBooking)
            var trovato = false
            ref.child("Prenotazione").observe(.value, with: {snapshot in
                for child in snapshot.children {
                    let child = child as! DataSnapshot
                    for child in child.children{
                        let child = child as! DataSnapshot
                        print(child.key)
                        if child.key == booking?.codeBooking && !trovato{
                            barButton.customView?.isUserInteractionEnabled = true
                            barButton.customView?.alpha = 1
                            UserDefaults.standard.set(true, forKey: "isBooked")
                            trovato = true
                        }
                        else {
                            barButton.customView?.isUserInteractionEnabled = false
                            barButton.customView?.alpha = 0.5
                            UserDefaults.standard.set(false, forKey: "isBooked")
                        }
                    }
                }
            })
            
        }
       /* if ( )
        {
            print("vero")
            barButton.customView?.isUserInteractionEnabled = true
            barButton.customView?.alpha = 1
        }
        else
        {
            print("falso")
            barButton.customView?.isUserInteractionEnabled = false
            barButton.customView?.alpha = 0.5
        }*/
        navigationItem.rightBarButtonItem = barButton
        
        
        
    }
    
    
    
    @IBAction func applyFilters(_ sender: Any) {
        if !filterApplied{
            filterView.backgroundColor = .orange
            check.image = UIImage(named: "whiteCheck")
            filteredActivity = searchedActivity.filter{$0.onlineBookings == true}
            filterApplied = true
            
        } else {
            filterApplied = false
            filterView.backgroundColor = .white
            check.image = UIImage(named: "blackCheck")
            filteredActivity = [Activity]()
            
        }
        tableview.reloadData()
        
    }
    
    @objc func showAlert(){
        let alert = AlertServices()
        self.present(alert.alert("ConnectionAlert"), animated: true, completion: nil)
    }
    
    @objc func showBooks(){
        self.performSegue(withIdentifier: "openBooking", sender: self)
    }
    
    @objc func openMaps(sender: UIButton){
        print("tap")
        NotificationCenter.default.addObserver(self, selector: #selector(mapsConfirmed), name: Notification.Name("openMapsConfirmed"), object: nil)
        let alert = AlertServices()
        self.present(alert.alert("mapsAlert"), animated: true, completion: {
            self.cellTapped = sender.tag
        })
    }
    
    @objc func mapsConfirmed(){
        searchedActivity[cellTapped].openPosition()
    }
    
    @objc func book(sender: UIButton){
        print("book")
        var act : Activity!
        if filterApplied {
            act = filteredActivity[sender.tag]
        }
        else {
            act = searchedActivity[sender.tag]
        }
        codeActivity = act.code
        nameActivity = act.name
        onlineActivity = act.onlineBookings
        
        self.performSegue(withIdentifier: "booking", sender: self)
    }
    
    @IBAction func openCamera(_ sender: Any) {
        performSegue(withIdentifier: "openCamera", sender: sender)
    }
    
    
    @objc func joinQ(_ notification : Notification) {
        let code = notification.userInfo!["Code"] as! String
        
        if (checkCode(codeDetected: code)){
            self.performSegue(withIdentifier: "booking", sender: self)
        }
        else {
            let alert = AlertServices()
            self.present(alert.alert("qrError"),animated: true, completion: nil)
        }
        
    }
    
    
    
    func checkCode(codeDetected : String) -> Bool  {
        
        for activity in activities {
            if activity.code == codeDetected {
                codeActivity = codeDetected
                nameActivity = activity.name!
                return true
            }
        }
        return false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "booking" {
            let vc = segue.destination as! JoinQViewController
            vc.nameActivity = nameActivity
            vc.codeActivity = codeActivity
            vc.isOnline = onlineActivity
        } else if segue.identifier == "openCamera" {
            let button = sender as! UIButton
            let segue = (segue as! OHCircleSegue)
            segue.destination.modalPresentationStyle = .fullScreen
            segue.circleOrigin = button.center
        } else if segue.identifier == "openBooking" {
            let vc = segue.destination as! LeaveQueueViewController
            vc.booking = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.data(forKey: "Booking")!) as? Booking
            
        }
    }
    
    
    
    //There, I have to get the list of all the activities
    
    
}

extension ViewController : UITableViewDataSource,UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        
        if filterApplied {
            return searchedActivity.filter{$0.onlineBookings == true}.count
        }
        
        return searchedActivity.count
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tapped {
            guard let cell = tableView.cellForRow(at: indexPath) as? CustomTableViewCell else {
                return 175
            }
            
            if cell.isExpandend {
                cell.arrowInfo.transform = CGAffineTransform(rotationAngle: 3.14/2)
                return 275
            } else {
                cell.arrowInfo.transform = CGAffineTransform(rotationAngle: 0)
                return 175
            }
            
        }
        
        
        return 175
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! CustomTableViewCell
        
        tapped = true
        if cell.isExpandend {
            //            cell.arrowInfo.transform = CGAffineTransform(rotationAngle: 0)
            cell.isExpandend = false
        } else {
            //            cell.arrowInfo.transform = CGAffineTransform(rotationAngle: 3.14/2)
            cell.isExpandend = true
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "custom cell") as! CustomTableViewCell
        var act : Activity!
        
        if filterApplied  {
            act = searchedActivity.filter{$0.onlineBookings == true}[indexPath.section]
        } else {
            act = searchedActivity[indexPath.section]
        }
        
        
        
        cell.arrowInfo.transform = CGAffineTransform(rotationAngle: 0)
        cell.isExpandend = false
        //        cell.ResturantImage.layer.cornerRadius = cell.ResturantImage.frame.height/6
        //        cell.ResturantImage.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        let url = act.image!
        let image = storage.child("images").child(url)
        
        image.getData(maxSize: 4*1024*1024, completion: {(data,error) in
            if let error = error {
                print(error)
                return
            }
            if let data = data {
                cell.ResturantImage.image = UIImage(data: data)
            }
        })
        
        if (act.onlineBookings){
            cell.BookBtn.setImage(UIImage(named:"bookButton"), for: .normal)
        }
        else
        {
            cell.BookBtn.setImage(UIImage(named:"checkButton"), for: .normal)
        }
        
        cell.MapButton.tag = indexPath.section
        cell.MapButton.addTarget(self, action: #selector(openMaps), for: .touchUpInside)
        cell.ResturantName.text = act.name!
        cell.Time.text = "\(act.openingTime!) - \(act.closingTime!) "
        cell.BookBtn.tag = indexPath.section
        cell.BookBtn.addTarget(self, action: #selector(book), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layer.masksToBounds = true
        DispatchQueue.main.async {
            cell.layoutSubviews()
        }
    }
}

extension ViewController :  UISearchBarDelegate, UISearchTextFieldDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if filterApplied {
            searchedActivity = searchText.isEmpty ? activities.filter{$0.onlineBookings == true} : filteredActivity.filter{$0.name.contains(searchText)}
            
        } else
        {
            searchedActivity = searchText.isEmpty ? activities : activities.filter{$0.name.contains(searchText)}
        }
        tableview.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchBar.endEditing(true)
        super.touchesEnded(touches, with: event)
    }
}

extension UIButton {
    
    func CreateFloatingButton () {
        layer.cornerRadius = frame.height/2
        layer.shadowOpacity = 0.25
        layer.shadowRadius = 5
        layer.shadowOffset = CGSize(width: 0, height: 10)
        
    }
}
