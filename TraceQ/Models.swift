//
//  Models.swift
//  TraceQ
//
//  Created by Danilo Casillo on 12/05/2020.
//  Copyright © 2020 Danilo Casillo. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class Position : NSObject{
    var citta : String!
    var via : String!
    var url : String!
  /*  var latitude: CLLocationDegrees!
    var longitude: CLLocationDegrees!
   */
    init(citta : String, via: String, url: String){
        self.citta = citta
        self.via = via
        self.url = url
    }
    
   /* func getMapItem() -> MKMapItem {
        let coordinates = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let placeMark = MKPlacemark(coordinate: coordinates,addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placeMark)
        self.citta = placeMark.locality
        self.via = placeMark.subLocality
        return mapItem
    }*/
    
    
}

class Activity: NSObject {
    var code : String!
    var name : String!
    var image : String!
    var onlineBookings : Bool!
    var openingTime : String!
    var closingTime : String!
    var groupLimit : Int!
    var position : Position!
    var type : String!
    
    init(code: String, name: String,image: String, type: String, citta: String, via: String, url: String, onlineBookings : Bool, openingTime: String, closingTime: String, groupLimit: Int){
        self.image = image
        self.code = code
        self.type = type
        self.name = name
        self.position = Position(citta: citta, via: via, url: url)
        self.onlineBookings = onlineBookings
        self.openingTime = openingTime
        self.closingTime = closingTime
        self.groupLimit = groupLimit
    }
    
    func openPosition(){
        if let url = URL(string: self.position.url)
        {
             UIApplication.shared.open(url)
        }
    }
    
}

class Booking : NSObject, NSCoding {
    
    var codeActivity : String!
    var nameBooker : String!
    var numPeople : Int
    var codeBooking : String!
    
    init(codeActivity: String, nameBooker: String, numPeople: Int, codeBooking: String){
        self.codeActivity = codeActivity
        self.nameBooker = nameBooker
        self.numPeople = numPeople
        self.codeBooking = codeBooking
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(codeActivity!,forKey: "codeActivity")
        coder.encode(nameBooker!,forKey: "nameBooker")
        coder.encode(numPeople,forKey: "numPeople")
        coder.encode(codeBooking!,forKey: "codeBooking")
    }
    
    required init?(coder: NSCoder) {
        self.codeActivity = coder.decodeObject(forKey: "codeActivity") as! String
        self.nameBooker = coder.decodeObject(forKey: "nameBooker") as! String
        self.codeBooking = coder.decodeObject(forKey: "codeBooking") as! String
        self.numPeople = coder.decodeInteger(forKey: "numPeople")
        
    }
    

}
