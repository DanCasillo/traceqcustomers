//
//  ConnectionAlertViewController.swift
//  TraceQ
//
//  Created by Danilo Casillo on 04/06/2020.
//  Copyright © 2020 Danilo Casillo. All rights reserved.
//

import UIKit

class ConnectionAlertViewController: UIViewController {
    @IBOutlet weak var NoConnection: UIImageView!
    @IBOutlet weak var NoConnectionView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NoConnectionView.clipsToBounds = true
        NoConnectionView.layer.cornerRadius = NoConnectionView.layer.frame.height/6
        
        NotificationCenter.default.addObserver(self, selector: #selector(dismissVC), name: Notification.Name("dismissAlert"), object: nil)
        // Do any additional setup after loading the view.
        NoConnectionView.layer.isOpaque = true
        NoConnectionView.layer.borderWidth = 3
        NoConnectionView.layer.borderColor = #colorLiteral(red: 1, green: 0.6588235294, blue: 0.1294117647, alpha: 1)
//            CGColor(srgbRed: 255/255, green: 168/255, blue: 33/255, alpha: 1)
        
    }
    
    
    @objc func dismissVC(){
        self.dismiss(animated: true, completion: nil)
    }
    
}

class errorAlertViewController: UIViewController {
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var alertImage: UIImageView!
    @IBOutlet weak var ok1: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        alertView.clipsToBounds = true
        alertView.layer.cornerRadius = alertView.frame.height/6
        alertView.layer.borderWidth = 3
        alertView.layer.borderColor = #colorLiteral(red: 1, green: 0.2131972611, blue: 0.07712707669, alpha: 1)
        
        
    }
    
    
    @IBAction func didTapCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

class qrAlertViewController: UIViewController {
    @IBOutlet weak var qrAlertView: UIView!
    @IBOutlet weak var QRimage: UIImageView!
    @IBOutlet weak var ok2: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        qrAlertView.clipsToBounds = true
        qrAlertView.layer.isOpaque = false
        qrAlertView.layer.cornerRadius = qrAlertView.layer.frame.height/6
        qrAlertView.layer.borderWidth = 3
        qrAlertView.layer.borderColor = #colorLiteral(red: 1, green: 0.2131972611, blue: 0.07712707669, alpha: 1)
    }
    
    @IBAction func didTapCencel1(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

class leavequeueViewController: UIViewController {
    @IBOutlet weak var queueView: UIView!
    @IBOutlet weak var queueimage: UIImageView!
    @IBOutlet weak var cancel: UIButton!
    @IBOutlet weak var leave: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        queueView.clipsToBounds = true
        queueView.layer.cornerRadius = queueView.layer.frame.height/6
        
        queueView.layer.borderWidth = 3
        queueView.layer.borderColor = #colorLiteral(red: 1, green: 0.6588235294, blue: 0.1294117647, alpha: 1)
        
    }
    
    @IBAction func CancelAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func LeaveAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "leavingConfirmed"), object: nil)
        })
    }
    
}

class openMapViewController: UIViewController {
    @IBOutlet weak var mapsView: UIView!
    @IBOutlet weak var mapImage: UIImageView!
    @IBOutlet weak var `continue`: UIButton!
    @IBOutlet weak var cancel: UIButton!
    
    var linkMaps : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapsView.clipsToBounds = true
        mapsView.layer.cornerRadius = mapsView.layer.frame.height/6
        
        mapsView.layer.borderWidth = 3
        mapsView.layer.borderColor = #colorLiteral(red: 1, green: 0.6588235294, blue: 0.1294117647, alpha: 1)
    }
    
    @IBAction func CancelAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func ContinueAction(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "openMapsConfirmed"), object: nil, userInfo: ["link" : self.linkMaps])
            
        })
    }
    
}

