//
//  AlertServices.swift
//  TraceQ
//
//  Created by Danilo Casillo on 04/06/2020.
//  Copyright © 2020 Danilo Casillo. All rights reserved.
//

import UIKit

class AlertServices {

    
    var alertShowed : Bool = false
    
    func alert(_ viewControllerID : String) -> UIViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: .main)
        
        let alertVC = storyboard.instantiateViewController(withIdentifier: viewControllerID)
        
        return alertVC
        
    }
}
