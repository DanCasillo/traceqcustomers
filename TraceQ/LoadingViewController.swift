//
//  LoadingViewController.swift
//  TraceQ
//
//  Created by Danilo Casillo on 01/06/2020.
//  Copyright © 2020 Danilo Casillo. All rights reserved.
//

import UIKit
import Firebase
import Reachability

//NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)

class LoadingViewController: UIViewController {
    
      var ud = UserDefaults.standard
    var reference : DatabaseReference!
    var activities = [Activity]()
    var alertService : AlertServices!
    @IBOutlet weak var ominiLoading: UIImageView!
    
    
    var ominiImages : [UIImage] = [UIImage(named: "Omini-1")!,UIImage(named: "Omini-2")!,UIImage(named:"Omini-3")!]
    
    var hasLoaded : Bool = false
    
    var networkReachability : Reachability!
    
    
     
    
    override func viewDidAppear(_ animated: Bool) {
        
        ominiLoading.animationImages = ominiImages
        ominiLoading.animationDuration = 1.27
        ominiLoading.startAnimating()
        networkReachability = try! Reachability()
        
        do {
            try networkReachability.startNotifier()
        } catch {
            print("Unable to start notification")
        }
        
        if networkReachability.connection == .unavailable
        {
            NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "showAlert")))
        } else  {
            loadActivities()
            
        }
        NotificationCenter.default.addObserver(self, selector: #selector(checkReachability), name: .reachabilityChanged, object: nil)
        
    }
    
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(showAlert), name: Notification.Name("showAlert"), object: nil)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("showAlert"), object: nil)
    }
    
    
    
    @objc func checkReachability(){
        
        networkReachability.whenReachable = {reachability in
            if reachability.connection != .unavailable {
                if !self.hasLoaded {
                    self.loadActivities()
                }
            }
            
            NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "dismissAlert")))
            
        }
        
        networkReachability.whenUnreachable = { _ in
            NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "showAlert")))
        }
        
    }
    
    func loadActivities(){
        self.loadValue(){success in
            if (success)
            {
                print("finito")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "RootViewController") as? UINavigationController
                let child = vc?.children[0] as! ViewController
                child.activities = self.activities
                let transition: CATransition = CATransition()
                transition.duration = 0.5
                transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                transition.type = CATransitionType.fade
                transition.subtype = CATransitionSubtype.fromBottom
                self.view.window!.layer.add(transition, forKey: nil)
                self.dismiss(animated: false, completion: {
                    self.view.window?.rootViewController = vc
                })
                self.view.window?.rootViewController = vc
                self.hasLoaded = true
            }
        }
    }
    
    @objc func showAlert(){
        let alert = AlertServices()
        self.present(alert.alert("ConnectionAlert"), animated: true, completion: nil)
    }
    
    
    func loadValue(completion: @escaping (Bool) -> Void){
        let reference = Database.database().reference()
        
        reference.child("Attività").observe(.value, with: {snapshot in
            
            for child in snapshot.children {
                let child = child as! DataSnapshot
                var type : String = child.key
                print("type: \(type)")
                for child in child.children
                {
                    let snapshot = child as! DataSnapshot
                    print(snapshot.key)
                    let val = snapshot.value as! NSDictionary
                    
                    
                    let attivita = Activity(code: snapshot.key, name: val["Nome"] as! String,image: val["Immagine"] as! String,type: type,citta: val["Citta"] as! String,via: val["Via"] as! String , url: val["URL"] as! String, onlineBookings: val["Online"] as! Bool, openingTime: val["openingTime"] as! String, closingTime: val["closingTime"] as! String, groupLimit: val["groupLimit"] as! Int)
                    
                    self.activities.append(attivita)
                    
                }
            }
            
            
            completion(true)
        })
        
        /*reference.child("Attività").observe(.childAdded, with: {snapshot in
         
         var type : String = snapshot.key
         print("type: \(type)")
         for child in snapshot.children
         {
         let snapshot = child as! DataSnapshot
         print(snapshot.key)
         let val = snapshot.value as! NSDictionary
         
         
         let attivita = Activity(code: snapshot.key, name: val["Nome"] as! String,image: val["Immagine"] as! String,type: type,citta: val["Citta"] as! String,via: val["Via"] as! String , url: val["URL"] as! String, onlineBookings: val["Online"] as! Bool, openingTime: val["openingTime"] as! String, closingTime: val["closingTime"] as! String, groupLimit: val["groupLimit"] as! Int)
         
         self.activities.append(attivita)
         
         }
         
         
         completion(true)
         })*/
    }

    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
