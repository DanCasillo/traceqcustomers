//
//  CustomTableViewCell.swift
//  TraceQ
//
//  Created by Michele Garofalo on 14/05/2020.
//  Copyright © 2020 Danilo Casillo. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    
    @IBOutlet weak var ResturantImage:UIImageView!
    @IBOutlet weak var ResturantName: UILabel!
    @IBOutlet weak var ResturantAddress: UILabel!
    @IBOutlet weak var MapButton: UIButton!
    @IBOutlet weak var ClockPic: UIImageView!
    @IBOutlet weak var Time: UILabel!
    @IBOutlet weak var BookBtn: UIButton!
    @IBOutlet weak var arrowInfo: UIImageView!
    
    var isExpandend : Bool = false
    var codeActivity : String! 
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   
    
    override func layoutSubviews() {
        configureShodow()
        super.layoutSubviews()
    }
    
    func configureShodow() {
        contentView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        
        selectionStyle = .none
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 10)
        layer.shadowRadius = 6.0
        layer.shadowOpacity = 0.5
        layer.masksToBounds = false
        clipsToBounds = false
    }
    
}
