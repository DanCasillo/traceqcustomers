//
//  InitialViewController.swift
//  TraceQ
//
//  Created by Danilo Casillo on 16/06/2020.
//  Copyright © 2020 Danilo Casillo. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {

    var ud = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(ud.object(forKey: "firstLaunch") == nil){
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "Tutorial") as? TutorialViewController
            self.view.window?.rootViewController = vc
        }
        else
        {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "Loading") as? LoadingViewController
            self.view.window?.rootViewController = vc
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
