//
//  JoinQViewController.swift
//  TraceQ
//
//  Created by Francesco Aroldo on 26/05/2020.
//  Copyright © 2020 Danilo Casillo. All rights reserved.
//

import UIKit
import Firebase

class JoinQViewController: UIViewController {
    
    @IBOutlet weak var TFName: UITextField!
    @IBOutlet weak var buttonMeno: UIButton!
    @IBOutlet weak var buttonPiu: UIButton!
    @IBOutlet weak var manyPeople: UILabel!
    @IBOutlet weak var numberView: UIView!
    @IBOutlet weak var notOnlineText: UILabel!
    
    @IBOutlet weak var num: UILabel!
    @IBOutlet weak var joinQ: UIButton!
    @IBOutlet weak var nameRestaurant: UILabel!
    
    
    //Data elements
    var ref: DatabaseReference!
    var codeActivity : String!
    var nameActivity : String!
    var previousBooking : Int = 0
    var isOnline : Bool = false
    
    
    var people: Int = 1 {
        didSet {
            if people == 1 {
                self.buttonMeno.alpha = 0.5
                self.buttonMeno.isUserInteractionEnabled = false
                
                
            } else {
                self.buttonMeno.alpha = 1
                self.buttonMeno.isUserInteractionEnabled = true
            }
        }
    }
    
    override func viewDidLoad() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(showAlert), name: Notification.Name("booking"), object: nil)
        
        ref = Database.database().reference()
        nameRestaurant.text = nameActivity!
        super.viewDidLoad()
        
        tapGesture()
        joinQ.isHidden = !isOnline
        notOnlineText.isHidden = isOnline
        
    }
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(showAlert), name: Notification.Name("showAlert"), object: nil)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("showAlert"), object: nil)
    }
    
    @IBAction func addPeople(_ sender: Any) {
        buttonMeno.isUserInteractionEnabled = true
        people += 1
        self.manyPeople.text = "\(people)"
        
        getBookings(completion: {(isFinished,value) in
            if isFinished {
                self.num.text = String(value+1)
            }
        })
        
    }
    
    @IBAction func removePeople(_ sender: Any) {
        people -= 1
        self.manyPeople.text = "\(people)"
        
        getBookings(completion: {(isFinished,value) in
            if isFinished {
                self.num.text = String(value+1)
            }
        })
        
    }
    
    private func tapGesture(){
        let tapGesturez = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        view.addGestureRecognizer(tapGesturez)
    }
    
    @objc func handleTap(){
        view.endEditing(true)
    }
    
    @IBAction func done1(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
    
    @IBAction func TFEdit(_ sender: UITextField) {
        if !sender.text!.isEmpty {
            
            self.joinQ.alpha = 1
            self.joinQ.isUserInteractionEnabled = true
            getBookings(completion: {(isFinished,value) in
                if isFinished {
                    self.num.text = String(value+1)
                }
            })
        } else {
            self.joinQ.alpha = 0.5
            self.joinQ.isUserInteractionEnabled = false
        }
        //        joinQ.alpha = 1.0
        //        joinQ.isUserInteractionEnabled = true
        numberView.isHidden = false
        //        num.isHidden = false
        
    }
    
    @IBAction func joinQclicked(_ sender: UIButton) {
        
        sender.isUserInteractionEnabled = false
        
        if (UserDefaults.standard.bool(forKey: "isBooked"))
        {
            /*let alert = UIAlertController(title: "ERROR!", message: "You already have an active booking.  ", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Home", style: UIAlertAction.Style.cancel, handler: {(action) in
                
                self.navigationController?.popToRootViewController(animated: true)
                
            } )
            
            alert.addAction(cancelAction)
            
            */
            
            let alert = AlertServices()
            self.present(alert.alert("errorAlertViewController"), animated: true, completion: nil)
            return
        }
        
        let actualDate = Date()
        
        let df = DateFormatter()
        df.dateFormat = "dd hh:mm:ss a"
        
        
        let now = df.string(from: actualDate)
        print(now)
        
        
        let ud = UserDefaults.standard
        
        //Add book into database
        ref.child("Prenotazione").child(codeActivity!).childByAutoId().setValue(["Nome" : TFName.text! as NSString, "Numero": Int(self.manyPeople.text!)! as NSNumber, "Orario" : now as NSString, "Token": ud.string(forKey: "deviceToken")! as NSString], withCompletionBlock: {(error,ref) in
            
            if let _ = error {
                print("errore")
            } else {
                //if any error doesn't occures, come back to root
                
                ud.set(true, forKey: "isBooked")
                let booking = Booking(codeActivity: self.codeActivity!, nameBooker: self.TFName.text!, numPeople: Int(self.manyPeople.text!)! , codeBooking: ref.key!)
                
                let encodedBooking : Data = NSKeyedArchiver.archivedData(withRootObject: booking)
                
                ud.set(encodedBooking, forKey: "Booking")
                
                self.navigationController?.popToRootViewController(animated: true)
            }
            
        })
    }
    
    @objc func showAlert(){
        let alert = AlertServices()
        self.present(alert.alert("ConnectionAlert"), animated: true, completion: nil)
    }
    
    
}

extension JoinQViewController {
    
    func getBookings(completion: @escaping (Bool, Int) -> Void){
        
        print("entro qua")
        var totalBooks : Int = 0
        print(self.num.text!)
        ref.child("Prenotazione").child(codeActivity).observe(.childAdded, with: {snapshot in
            print(snapshot)
            let booking = snapshot.value as! NSDictionary
            print(booking["Numero"])
            if (booking["Numero"] as! Int) == Int(self.manyPeople.text!) {
                
                print(totalBooks)
                totalBooks += 1
            }
            completion(true,totalBooks)
        })
        
    }
}
